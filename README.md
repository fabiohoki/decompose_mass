decompose_mass
==============

Shiny app to decompose a mass to possible molecular formulas

Live at http://predret.com/tools/mass-decomposition/.
